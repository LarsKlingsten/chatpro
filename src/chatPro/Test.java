package chatPro;

public class Test {
 
	public static void main(String[] args) throws Exception {
		ChatClient lk = new ChatClient("Lars");
		lk.start();
		
		ChatClient md = new ChatClient("Morten");
		md.start();
		
		ChatClient sp = new ChatClient("Steffen");
		sp.start();
	}
}