package chatPro;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress; 

public class BroadCastServer extends Thread{

	private DatagramSocket server;
	private final int PORT = 11001;

	public void broadCastServer() throws Exception{
		byte[] receiveByte = new byte[1024];
		server = new DatagramSocket(PORT);
		server.setBroadcast(true);
		System.out.println("BroadCastServer listening on port: " + PORT);
		DatagramPacket receivePacket = new DatagramPacket(receiveByte, receiveByte.length);

		while(true){
			server.receive(receivePacket);
			System.out.println("Received information from " + receivePacket.getAddress().getHostAddress());

			byte[] send = InetAddress.getLocalHost().getHostAddress().getBytes();
			DatagramPacket sendPackage = new DatagramPacket(send, send.length, receivePacket.getAddress(), receivePacket.getPort());
			try {
				server.send(sendPackage);
				System.out.println("Package have been sent to " + receivePacket.getAddress().getHostAddress());
				System.out.println("");
			} catch (IOException e) {
				System.out.println("Not possible to send package");
				e.printStackTrace();
			}

			receivePacket.setLength(receiveByte.length);
		}
	}
	public void run() {
		
		System.out.println("its running");
		try {
			broadCastServer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
