package chatPro;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class BroadCastClient {
	
 	public String broadCastClient() throws Exception{
		DatagramSocket socket = new DatagramSocket();
		socket.setBroadcast(true);
		socket.setSoTimeout(2000);
		DatagramPacket dgram;
		int numberOfTimesTried = 0;
		String received = "";

		while(numberOfTimesTried < 3){
			try{
				byte[] b = new byte[1];
				dgram = new DatagramPacket(b, b.length, InetAddress.getByName("255.255.255.255"), 11001);
				socket.send(dgram);
				System.out.println("Package have been sent, now waiting for responds");

				byte[] receive = new byte[1024];
				DatagramPacket receivePackage = new DatagramPacket(receive, receive.length);
				socket.receive(receivePackage);

				received = new String(receivePackage.getData()).trim();
				System.out.println("Package have been received, ip of server is " + received);
				numberOfTimesTried = 3;

			}catch(SocketTimeoutException ste){
				System.out.println("Problem getting response, trying one more time");
				numberOfTimesTried++;
			}
		}
		socket.close();
		return received;
	}
	 
}
